/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pf_buffer.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/06 17:29:32 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/15 18:43:44 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include "lft_io.h"
#include "ft_printf_internals.h"

void		pf_buffer_flush(t_arg *arg)
{
	if (write(STDOUT_FILENO, arg->buffer, arg->buff_len) == -1)
		arg->flags |= FLAG_OUTPUT_ERROR;
	arg->buff_len = 0;
}

void		pf_buffer_fill(t_arg *arg, void *src, size_t len)
{
	const char *tmp = (char *)src;

	while (len != 0 && arg->buff_len < PRINTF_BUFFER_SIZE)
	{
		len--;
		arg->buffer[arg->buff_len++] = *tmp++;
	}
	if (arg->buff_len == PRINTF_BUFFER_SIZE)
	{
		pf_buffer_flush(arg);
		if (len != 0)
			pf_buffer_fill(arg, (void *)tmp, len);
	}
}
