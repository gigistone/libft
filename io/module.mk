INC += io/lft_io.h

SRC += io/ft_gnl.c
SRC += io/ft_putchar.c
SRC += io/ft_putchar_fd.c
SRC += io/ft_putendl.c
SRC += io/ft_putendl_fd.c
SRC += io/ft_putnbr.c
SRC += io/ft_putnbr_fd.c
SRC += io/ft_putstr.c
SRC += io/ft_putstr_fd.c


SRC += io/printf/ft_printf.c
SRC += io/printf/pf_buffer.c
SRC += io/printf/pf_display.c
SRC += io/printf/pf_eval_attr.c
SRC += io/printf/pf_eval_spec.c
SRC += io/printf/pf_floats.c
SRC += io/printf/pf_format_num.c
SRC += io/printf/pf_format_text.c
SRC += io/printf/pf_integers.c
SRC += io/printf/pf_numerics.c
SRC += io/printf/pf_utils.c
