/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_io.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 23:15:18 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/15 18:43:40 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_IO_H
# define LFT_IO_H

# define BUFF_SIZE			(3)
# define GNL_ALLOC_ERROR	(-1)
# define LINE_BUFFER_EMPTY	(0)

enum	e_gnl_state
{
	GNL_ERROR = -1,
	GNL_EOF,
	GNL_SUCCESS
};

# define PRINTF_BUFFER_SIZE (64)
# define PRINTF_OUTPUT_ERROR (-1)

void	ft_putchar(char c);
void	ft_putstr(char const *str);
void	ft_putendl(char const *str);
void	ft_putchar_fd(char c, int fd);
void	ft_putstr_fd(char const *s, int fd);
void	ft_putendl_fd(char const *s, int fd);
void	ft_putnbr(int n);
void	ft_putnbr_fd(int n, int fd);
int		ft_gnl(const int fd, char **line);
int		ft_printf(const char *format, ...);

#endif
