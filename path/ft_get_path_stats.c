/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_path_stats.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/18 15:47:53 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/18 15:49:17 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_path.h"
#include <sys/stat.h>

int		ft_get_path_stats(const char *path, t_path_stat	*stats)
{
	char	**splitted_path = ft_strsplit(path, '/');
	size_t	path_len;
	struct stat	file_stats;

	ft_bzero(stats, sizeof(stats));
	if ((path_len = ft_str_arr_len((const char **)splitted_path)) == 1)
	{
		ft_strcpy(stats->last_dir, splitted_path[0]);
		return 0;
	}
	if (path_len == 0)
	{
		ft_strcpy(stats->last_dir, (*path == '/') ? "/" : "");
		return 0;
	}
	else
	{
		stat(path, &file_stats);
		if (S_ISDIR(file_stats.st_mode))
			ft_strcpy(stats->last_dir, splitted_path[path_len - 1]);
		else
		{
			ft_strcpy(stats->last_dir, splitted_path[path_len - 2]);
			ft_strcpy(stats->file_name, splitted_path[path_len - 1]);
		}
	}
	return 0;
}