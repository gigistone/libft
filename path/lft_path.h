/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_path.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 17:41:50 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/18 15:49:21 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_PATH_H
# define LFT_PATH_H

#include <stdio.h>
#include "lft_string.h"

typedef struct	s_path_stat {
	char	last_dir[FILENAME_MAX];
	char	file_name[FILENAME_MAX];
}				t_path_stat;

int	ft_combine_paths(const char *path1, const char *path2, char *output, size_t size);
int	ft_get_path_stats(const char *path, t_path_stat	*stats);

#endif
