/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_combine_paths.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/15 17:42:07 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/15 17:43:17 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_path.h"

int		ft_combine_paths(const char *path1, const char *path2, char *output, size_t size)
{
	ft_bzero(output, size);
	const	int s = ft_strlen(path1);

	ft_strlcat(output, path1, size);
	if (*(path1 + s - 1) == '/')
		ft_strlcat(output, path2, size);
	else
	{
		ft_strlcat(output, "/", size);
		ft_strlcat(output, path2, size);
	}
	return (0);
}
