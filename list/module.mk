INC += list/lft_list.h

SRC +=	list/ft_lstadd.c \
		list/ft_lst_at.c \
		list/ft_lstcount.c \
		list/ft_lstdel.c \
		list/ft_lstdelone.c \
		list/ft_lst_find.c \
		list/ft_lstiter.c \
		list/ft_lst_last.c \
		list/ft_lstmap.c \
		list/ft_lstnew.c
