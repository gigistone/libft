/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 23:18:20 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 23:24:11 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_list.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*lcpy;
	t_list	*cur;

	if (lst == NULL)
		return (NULL);
	lcpy = f(lst);
	cur = lcpy;
	lst = lst->next;
	while (lst != NULL)
	{
		cur->next = f(lst);
		lst = lst->next;
		cur = cur->next;
	}
	return (lcpy);
}
