/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_at.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/22 23:04:17 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 23:24:49 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_list.h"

t_list	*ft_lst_at(t_list *lst, unsigned int nb)
{
	while (--nb != 0 && lst != NULL)
		lst = lst->next;
	if (nb != 0)
		return (NULL);
	return (lst);
}
