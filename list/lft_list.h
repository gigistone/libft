/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_list.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 23:21:12 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/10 13:51:04 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_LIST_H
# define LFT_LIST_H

# include <string.h>

typedef struct			s_list
{
	void				*content;
	size_t				content_size;
	struct s_list		*next;
}						t_list;

t_list					*ft_lstnew(void const *content, size_t content_size);
void					ft_lstdelone(t_list **alst, void (*del)(void *,
						size_t));
void					ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void					ft_lstadd(t_list **alst, t_list *new);
void					ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list					*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));
t_list					*ft_lstget_elem_if(t_list *lst, int (*f)(t_list *elem));
size_t					ft_lstcount(t_list *lst);
t_list					*ft_lst_last(t_list *lst);
t_list					*ft_lst_at(t_list *lst, unsigned int nb);
t_list					*ft_lst_find(t_list *lst, void *data_ref,
						int (*cmp)());

#endif
