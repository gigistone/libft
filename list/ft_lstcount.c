/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstcount.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/22 22:31:57 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 23:23:36 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_list.h"

size_t	ft_lstcount(t_list *lst)
{
	size_t	len;

	len = 0;
	while (lst != NULL)
	{
		++len;
		lst = lst->next;
	}
	return (len);
}
