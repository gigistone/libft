/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_math.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 22:21:53 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/10 21:48:03 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_MATH_H
# define LFT_MATH_H

int		ft_pow(int nb, int pow);
int		ft_isprime(long nb);
long	ft_get_next_prime(long nb);

#endif
