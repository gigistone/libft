/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_prime.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/10 21:10:45 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/18 10:22:44 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <limits.h>
#include "lft_math.h"

long	ft_get_next_prime(long nb)
{
	if (nb < 2)
		return (2);
	if (++nb % 2 == 0)
		nb++;
	while (nb < LONG_MAX)
	{
		if (ft_isprime(nb))
			return (nb);
		nb += 2;
	}
	return (0);
}
