/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_index_of.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 17:27:09 by bboutoil          #+#    #+#             */
/*   Updated: 2019/10/29 17:07:24 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

/*
** name: ft_str_arr_len
** desc: return the size of an array of strings
** ret:  the count of the strings inside the array.
*/

size_t	ft_str_arr_len(const char *arr[])
{
	size_t	len;

	len = 0;
	while (*arr++ != NULL && ++len)
		;
	return (len);
}
