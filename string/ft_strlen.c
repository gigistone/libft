/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/30 22:02:02 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 21:02:37 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

size_t	ft_strlen(const char *s)
{
	const char	*cpy = s;

	while (*s)
		++s;
	return (size_t)(s - cpy);
}
