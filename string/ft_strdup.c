/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 16:40:52 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 21:01:26 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

char	*ft_strdup(const char *s)
{
	size_t	n;
	void	*cpy;

	n = ft_strlen(s) + 1;
	if (n > 0)
	{
		cpy = malloc(n * sizeof(char));
		if (cpy != NULL)
			return ((char *)ft_memcpy(cpy, s, n));
	}
	return (NULL);
}
