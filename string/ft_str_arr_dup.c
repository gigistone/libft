/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_dup.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 17:27:38 by bboutoil          #+#    #+#             */
/*   Updated: 2019/10/27 18:06:58 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

char	**ft_str_arr_dup(const char *src[])
{
	const size_t	len = ft_str_arr_len(src);
	char			**dst;

	dst = (char **)ft_memalloc((len + 1) * sizeof(char *));
	return (ft_str_arr_cpy(dst, src));
}
