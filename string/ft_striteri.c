/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striteri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 16:41:39 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 21:02:14 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

void	ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	const char	*begin = s;

	if (s != NULL && f != NULL)
	{
		while (*s)
		{
			f(s - begin, s);
			s++;
		}
	}
}
