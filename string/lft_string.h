/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lft_string.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/07 20:50:28 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/14 14:45:00 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LFT_STRING_H
# define LFT_STRING_H

# include <string.h>

void					*ft_memset(void *s, int c, size_t size);
void					ft_bzero(void *s, size_t n);
void					*ft_memcpy(void *dest, const void *src, size_t n);
void					*ft_memccpy(void *dest, const void *src, int c,
						size_t n);
void					*ft_memmove(void *dest, const void *src, size_t n);
void					*ft_memchr(const void *s, int c, size_t n);
int						ft_memcmp(const void *s1, const void *s2, size_t n);
size_t					ft_strlen(const char *str);
size_t					ft_strnlen(const char *s, size_t n);
char					*ft_strcpy(char *dest, const char *src);
char					*ft_strncpy(char *dest, const char *src, size_t n);
char					*ft_strcat(char *dest, const char *src);
char					*ft_strncat(char *dest, const char *src, size_t n);
size_t					ft_strlcat(char *dst, const char *src, size_t size);
int						ft_pow(int nb, int pow);
int						ft_strcmp(const char *s1, const char *s2);
int						ft_strncmp(const char *s1, const char *s2, size_t n);
char					*ft_strchr(const char *str, int c);
char					*ft_strrchr(const char *str, int c);
char					*ft_strstr(const char *haystack, const char *needle);
char					*ft_strnstr(const char *big, const char *little,
						size_t len);
char					*ft_strdup(const char *s);
char					*ft_strndup(const char *s, size_t n);
int						ft_atoi(const char *nptr);
void					*ft_memalloc(size_t size);
void					ft_memdel(void **ap);
char					*ft_strnew(size_t size);
void					ft_strdel(char **as);
void					ft_strclr(char *s);
void					ft_striter(char *s, void (*f)(char *));
void					ft_striteri(char *s, void (*f)(unsigned int, char *));
char					*ft_strmap(char const *s, char (*f)(char));
char					*ft_strmapi(char const *s, char(*f)(unsigned int,
						char));
int						ft_strequ(char const *s1, char const *s2);
int						ft_strnequ(char const *s1, char const *s2, size_t n);
char					*ft_strsub(char const *s, unsigned int start,
						size_t len);
char					*ft_strjoin(char const *s1, char const *s2);
char					*ft_strtrim(char const *s);
char					**ft_strsplit(char const *s, char c);
void					ft_strarr_del(char ***arr);
char					*ft_strrev(char *s);
char					*ft_itoa(int n);
size_t					ft_str_arr_len(const char *arr[]);
char					**ft_str_arr_dup(const char *src[]);
int						ft_str_arr_index_of(const char *s[], char *pattern);
int						ft_str_arr_replace_at(char *arr[], size_t index,
						const char *s);
char					**ft_str_arr_cpy(char **dst, const char **src);
char					**ft_str_arr_new(size_t len);
#endif
