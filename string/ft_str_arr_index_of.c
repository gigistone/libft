/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_index_of.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 17:27:09 by bboutoil          #+#    #+#             */
/*   Updated: 2019/10/29 17:07:24 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

int	ft_str_arr_index_of(const char *s[], char *pattern)
{
	int	i;

	i = 0;
	while (s[i] != NULL)
	{
		if (ft_strncmp(s[i], pattern, ft_strlen(pattern)) == 0)
			return (i);
		i++;
	}
	return (-1);
}
