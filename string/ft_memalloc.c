/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/27 19:20:17 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 20:57:13 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

void	*ft_memalloc(size_t size)
{
	void	*mem;

	if (size > 0)
	{
		if ((mem = malloc(size)) != NULL)
			return (ft_memset(mem, 0, size));
	}
	return (NULL);
}
