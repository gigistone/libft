/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_replace_at.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 17:28:42 by bboutoil          #+#    #+#             */
/*   Updated: 2019/10/27 17:30:47 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

int	ft_str_arr_replace_at(char *arr[], size_t index, const char *s)
{
	free(arr[index]);
	if ((arr[index] = ft_strdup(s)) == NULL)
		return (-1);
	return (0);
}
