/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_cpy.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/10/27 17:57:06 by bboutoil          #+#    #+#             */
/*   Updated: 2019/10/27 22:11:12 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"

char	**ft_str_arr_cpy(char **dst, const char **src)
{
	size_t	i;

	i = 0;
	while (src[i] != NULL)
	{
		if ((dst[i] = ft_strdup(src[i])) == NULL)
			return (NULL);
		i++;
	}
	dst[i] = NULL;
	return (dst);
}
