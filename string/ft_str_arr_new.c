/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_arr_new.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/14 14:37:38 by bboutoil          #+#    #+#             */
/*   Updated: 2019/11/14 14:44:03 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	**ft_str_arr_new(size_t len)
{
	char	**arr;

	if (len <= 0 || (arr = malloc(sizeof(char *) * len)) == NULL)
		return (NULL);
	return (arr);
}
