/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 16:52:22 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 21:39:51 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "lft_string.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	const char	*s = (char *)src;
	char		*d;

	d = (char *)dest;
	while (n-- > 0)
		*d++ = *s++;
	return (dest);
}
