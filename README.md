# LIBFT

A tiny swiss army knife for c-lang. 

# Headers

## lft_math
Defines common mathematical functions
## lft_io
Defines core input and output functions
## lft_ctype
Defines set of functions used to classify characters by their types or to convert between upper and lower case in a way that is independent of the used character set
## lft_string
Defines string-handling functions
## lft_path
Defines path-handling functions
## lft_list
Defines some dynamic data structure as linked lists, array list and other collection types.

## How to use

The use of this library collection is not recommended. It is only useful in a pedagogical framework within the school 42.