/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/26 17:52:57 by bboutoil          #+#    #+#             */
/*   Updated: 2019/03/07 21:11:46 by bboutoil         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "lft_string.h"
#include "lft_ctype.h"

int	ft_atoi(const char *nptr)
{
	int	nb;
	int	sign;

	nb = 0;
	while (*nptr && ft_isspace(*nptr))
		++nptr;
	sign = *nptr == '-' ? -1 : 1;
	if (*nptr == '-' || *nptr == '+')
		++nptr;
	while (*nptr && ft_isdigit(*nptr))
	{
		nb = nb * 10 + *nptr - '0';
		++nptr;
	}
	return (nb * sign);
}
