# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bboutoil <bboutoil@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/09 18:28:43 by bboutoil          #+#    #+#              #
#    Updated: 2019/11/15 17:45:09 by bboutoil         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME := libft.a
BUILD_DIR := build
MODULES := common io string list ctype math path

SRC:=
OBJ:=
DEP:=
INC:=


################################################################################

LINK := gcc
CC := gcc
CFLAGS:=
CFLAGS += -Wall -Wextra -Werror
CFLAGS += -MMD -MP
CFLAGS += $(patsubst %,-I%,$(MODULES))

AR := ar
ARFLAGS := rcvs

CP := cp -f
MKDIR := mkdir -p
include $(patsubst %,%/module.mk,$(MODULES))
OBJ = $(SRC:.c=.o)
DEP := $(OBJ:.o=.d)


################################################################################

.PHONY: all clean fclean re

all : $(BUILD_DIR) $(NAME)

$(BUILD_DIR):
	@echo "Warning: build dir doesnt exists. trying to create..."
	$(MKDIR) $@
	@echo ">>> Build dir created"

$(NAME): $(OBJ)
	@$(AR) $(ARFLAGS) $@ $?
	@echo "Generation: $(NAME) generated."
	@$(CP) $(NAME) $(BUILD_DIR)
	@$(CP) $(INC) $(BUILD_DIR)
	@echo "Post-build: $(NAME) and headers are copied in build directory."

%.o : %.c
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@$(RM) $(OBJ) $(DEP)
	@echo "Workspace: Objects and dependency files removed."

fclean: clean
	@$(RM) $(NAME) $(BUILD_DIR)/$(NAME)
	@echo "Workspace: $(NAME) removed."

re: fclean all


################################################################################

# dependencies 
# (have to be place after other rules)

-include $(DEP)